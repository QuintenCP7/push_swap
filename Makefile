# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: quinten <quinten@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/08/31 23:23:29 by quinten           #+#    #+#              #
#    Updated: 2018/08/31 23:28:59 by quinten          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

all :
	make -C ./push_swap_includes
	make -C ./checker_includes

clean :
	make -C ./push_swap_includes clean
	make -C ./checker_includes clean

fclean :
	make -C ./push_swap_includes fclean
	make -C ./checker_includes fclean

re :
	make -C push_swap_includes re
	make -C checker_includes re
